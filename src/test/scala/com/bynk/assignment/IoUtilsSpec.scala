package com.bynk.assignment

import java.io.StringReader

import org.scalatest.{FlatSpec, Sequential}

import scala.collection.parallel.mutable.ParArray

class IoUtilsSpec extends FlatSpec {

  val obj = new IoUtilsImpl()
  val path: String = getClass.getResource("/Files/").getPath

  Sequential
  "Utility" should "be able list files in a directory" in {

    assert(obj.listFiles(Array(path)).getOrElse(Nil).length === 2)
  }

  "Utility" should "return error if the provided path is not a directory" in {

    assert(obj.listFiles(Array("invalid path")) === Left(NotADirectory("Path is not a directory")))
  }

  "Utility" should "be able to load file data" in {
    val files = obj.listFiles(Array(path)).getOrElse(Nil)

    assert(obj.loadFileData(files).keys.toList.length === 2)
  }

  "Utility" should "be able to read input from console" in {
    val input = "created by rishabh"
    val res: Option[Array[String]] = Console.withIn(new StringReader(input)) {
      obj.readInput()
    }

    assert(res.getOrElse(Array.empty[String]) === input.split(" "))
  }

  "Utility" should "be able to search text in file" in {
    val input = "created"
    val files = obj.listFiles(Array(path)).getOrElse(Nil)
    val fileData = obj.loadFileData(files)

    val res = obj.performSearch(input, fileData)
    assert(res === List((true, "203.txt"), (true, "204.txt")))
  }

  "Utility" should "be able to score matching files" in {
    val input = "created by rishabh".split(" ")
    val res = obj.scoreFiles(ParArray((true, "203.txt"), (false, "203.txt"), (false, "203.txt"), (false, "204.txt"), (true, "204.txt"), (true, "204.txt")), input)
    assert(res === List(FinalOutput("204.txt", 66.6666), FinalOutput("203.txt", 33.3333)))
  }
}
