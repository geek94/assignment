package com.bynk.assignment

import java.io.File

import scala.collection.parallel.immutable.ParMap
import scala.collection.parallel.mutable.ParArray
import scala.io.{Source, StdIn}
import scala.math.pow
import scala.util.Try

case class FinalOutput(fileName: String, score: Double)

sealed trait ReadFileError

case class NotADirectory(mgs: String) extends ReadFileError

case object MissingArg extends ReadFileError

case class FileNotFound(t: Throwable) extends ReadFileError


trait IOUtils {

  def listFiles(args: Array[String]): Either[ReadFileError, List[File]]

  def loadFileData(files: List[File]): ParMap[String, Set[String]]

  def initiateSearching(fileData: ParMap[String, Set[String]]): Unit

  def readInput(): Option[Array[String]]

  def performSearch(inputString: String, fileData: ParMap[String, Set[String]]): List[(Boolean, String)]

  def scoreFiles(searchOutput: ParArray[(Boolean, String)], input: Array[String]): List[FinalOutput]
}

class IoUtilsImpl extends IOUtils {

  /** Lists all the file of the given directory
    *
    * @param args path of the directory
    * @return List of files in the directory or Error in case of invalid argument
    */
  def listFiles(args: Array[String]): Either[ReadFileError, List[File]] = {
    args.headOption.fold[Either[ReadFileError, List[File]]](
      Left(MissingArg)
    ) {
      path =>
        Try(new File(path)).fold(ex => Left(FileNotFound(ex)), file =>
          if (file.isDirectory) {
            Right(file.listFiles().toList)
          } else {
            Left(NotADirectory("Path is not a directory"))
          }
        )
    }
  }

  /** loads all the data of the available files
    *
    * @param files list of files
    * @return ParMap, where key->FileName and value->fileData
    */
  def loadFileData(files: List[File]): ParMap[String, Set[String]] = {
    files.par.map { file =>
      val data = Source.fromFile(file).getLines.flatMap(_.toLowerCase.split(" ")).toSet[String]
      file.getName -> data
    }.toMap
  }

  /** Initiates  searching recursively on the provided input
    *
    * @param fileData parMap, which holds data of all the available files
    * @return Unit,writes output to the console and exits the application in case of input[:quit]
    */
  def initiateSearching(fileData: ParMap[String, Set[String]]): Unit = {

    def iterate(): Unit = {
      readInput().fold(System.exit(0)) { input =>
        val finalOutput = input.par.flatMap { word =>
          performSearch(word, fileData)
        }
        scoreFiles(finalOutput, input).foreach { output => println(s"${output.fileName} : ${output.score}%") }
        iterate()
      }
    }

    iterate()
  }

  /** Reads input from the console
    *
    * @return input from the console
    */
  def readInput(): Option[Array[String]] = {
    println("Search >")

    val input = StdIn.readLine().trim.toLowerCase.split(" ")

    input.headOption.fold[Option[Array[String]]](None) {
      case ":quit" => None
      case _ => Some(input)
    }
  }

  /** Lists all the file of the given directory
    *
    * @param inputString input from the console
    * @param fileData    parMap, which holds data of all the available files
    * @return (true,fileName) if word is found in file, else (false,fileName)
    */
  def performSearch(inputString: String, fileData: ParMap[String, Set[String]]): List[(Boolean, String)] = {
    fileData.keysIterator.map { key =>
      (fileData.getOrElse(key, Set.empty[String]).contains(inputString), key)
    }
  }.toList

  /** Lists all the file of the given directory
    *
    * @param searchOutput ParArray, consisting of boolean[present in file OR not] and fileName
    * @param input        input from the console
    * @return List[FinalOutput],which gives us the file name and the matching score
    */
  def scoreFiles(searchOutput: ParArray[(Boolean, String)], input: Array[String]): List[FinalOutput] = {
    val stringLength = input.length

    searchOutput.groupBy(_._2).mapValues(output => output.count(_._1)).map { case (fileName, occurences) =>
      val score: Double = (occurences.toDouble / stringLength.toDouble) * 100
      FinalOutput(fileName, score.rounded(4))
    }.toList.sortWith(_.score > _.score).take(10)
  }

  /**
    * setting the precision of Double value upto 4 decimal places.
    */
  implicit class ExtendedDouble(n: Double) {
    def rounded(value: Int): Double = {
      val w = pow(10, value)
      (n * w).toLong.toDouble / w
    }
  }

}
