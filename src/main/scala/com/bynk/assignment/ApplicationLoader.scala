package com.bynk.assignment

object ApplicationLoader extends App {

  val ioUtils = new IoUtilsImpl()

  ioUtils.listFiles(args)
    .fold(
      println,
      file => ioUtils.initiateSearching(ioUtils.loadFileData(file))
    )
}
