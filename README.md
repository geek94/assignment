# Assignment

# For running the application use:
    -> sbt run {path to the directory}

# After Search being prompted on console:
    -> insert the text to be searched, press Enter

# To exit the application use following commnad:
    -> type [:quit], press enter

# For running Scoverage use following commands:
    -> sbt clean coverage test
    -> sbt coverageReport

# For running scalastyle use following commnad:
    -> sbt scalastyle

# Sample Input:
    Created by some artist named Rishabh

# Sample Output:
    205.txt : 60.0%
    localedef.txt : 60.0%
    tr823.txt : 60.0%
    vol04.iss0064-0118.txt : 60.0%
    204.txt : 60.0%
    416.txt : 60.0%
    vol08.iss0001-0071.txt : 60.0%
    203.txt : 60.0%
    sprof.txt : 40.0%
    425.txt : 40.0%


# Assumptions:
    1. Matching of word is case insensitive(i.e Bynk == bynk)
    2. Scoring logic is simple, (no.of occurrences of input words in a file / input Length) * 100